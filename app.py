# app.py
import os
import pymysql
import boto3

# Retrieve RDS and S3 credentials from environment variables
rds_host = os.environ.get('RDS_HOST')
db_username = os.environ.get('DB_USERNAME')
db_password = os.environ.get('DB_PASSWORD')
db_name = os.environ.get('DB_NAME')
s3_bucket_name = os.environ.get('S3_BUCKET_NAME')

# Initialize the RDS connection
def connect_to_rds():
    try:
        conn = pymysql.connect(host=rds_host, user=db_username, password=db_password, database=db_name)
        return conn
    except Exception as e:
        print(f"Error connecting to RDS: {e}")

# Connect to S3 and upload a sample file
def upload_to_s3(file_path):
    try:
        s3 = boto3.client('s3')
        s3.upload_file(file_path, s3_bucket_name, os.path.basename(file_path))
        print(f"File uploaded to S3 successfully.")
    except Exception as e:
        print(f"Error uploading file to S3: {e}")

if __name__ == "__main__":
    # Example usage
    connection = connect_to_rds()
    if connection:
        # Perform database operations as needed
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM your_table;")
        results = cursor.fetchall()
        print(f"Results from RDS: {results}")
        
        # Upload a sample file to S3
        upload_to_s3('sample.txt')
        
        # Close the RDS connection
        connection.close()

